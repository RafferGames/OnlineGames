# Online Games

## Table of Contents

- [Installation](#installation)
	- [Prerequisites](#prerequisites)
	- [Clone repository](#clone-repository)
	- [Create database](#create-database)
- [License](#license)


## Installation

### Prerequisites

The following things need to be installed on your computer:
- [Git](https://git-scm.com/downloads)
- Webserver
- PHP
- SQL-Server


### Clone repository

Clone this repository and its submodules into the document-root directory of your webserver.

__Linux__
```
sudo git clone https://gitlab.com/RafferGames/OnlineGames.git /srv/http --recurse-submodules
```


### Create database

Execute all SQL-files.

__Linux__
```
mysql -u root
source /srv/http/db/setup/create_database.sql
source /srv/http/db/setup/create_user.sql
source /srv/http/user/setup/create_table_users.sql
source /srv/http/games/poker/setup/create_tables_poker.sql
```


## License

Distributed under the [MIT License](https://gitlab.com/RafferGames/OnlineGames/-/blob/main/LICENSE).

